module gitee.com/golangx/hc

go 1.16

require (
	github.com/BurntSushi/toml v0.3.1 // indirect
	github.com/armon/go-metrics v0.3.9 // indirect
	github.com/fatih/color v1.12.0 // indirect
	github.com/gomodule/redigo v1.8.5
	github.com/hashicorp/consul/api v1.9.1
	github.com/hashicorp/go-cleanhttp v0.5.2 // indirect
	github.com/hashicorp/go-hclog v0.16.1 // indirect
	github.com/hashicorp/go-immutable-radix v1.3.1 // indirect
	github.com/hashicorp/golang-lru v0.5.4 // indirect
	github.com/jmoiron/sqlx v1.3.4
	github.com/mattn/go-isatty v0.0.13 // indirect
	github.com/mitchellh/mapstructure v1.4.1 // indirect
	go.uber.org/atomic v1.8.0 // indirect
	go.uber.org/multierr v1.7.0 // indirect
	go.uber.org/zap v1.18.1
	golang.org/x/sys v0.0.0-20210630005230-0f9fa26af87c // indirect
	gopkg.in/natefinch/lumberjack.v2 v2.0.0
)
